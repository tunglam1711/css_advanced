Background
Cho phép add nhiều ảnh nền vào 1 element bằng thuộc tính background-image
Các ảnh nềN khác nhau phân cách bằng dấu phẩy, xếp chồng lên nhau và ảnh đầu tiên sẽ được hiển thị
background-image: chỉ định ảnh nền
background-position: chỉ định vị trí ảnh
background-repeat: cho phép dãn, lặp lại
background: là viết tắt cho 3 thuộc tính trên
background-size: xác định size ảnh nền, có thể theo độ dài, %, hoặc theo từ khoá contain, cover
background-origin: chỉ định vị trí hình nền, gồm 3 giá trị 
   border-box: ảnh nền bắt đầu từ góc trên bên trái
   padding-box: mặc định, bắt đầu từ góc trên bên trái của padding
   content-box: bắt đầu từ góc trên bên trái của content
background-clip: chỉ định phần được vẽ của nền
   border-box : (mặc định) nền được vẽ ở mép ngoài của đường viền 
   padding-box : được vẽ ở cạnh ngoài của phần padding
   content-box : được vẽ trong hộp nội dung
