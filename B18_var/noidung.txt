var(): là hàm được dùng để thêm giá trị của 1 biến CSS. Thya vì phải copy paste nhiều lần thì có thể dùng var() để thay thế
Cú pháp:   var(--name, value)
VD: 
      :root {
      --blue: #1e90ff;
      --white: #ffffff;
      }

      body { background-color: var(--blue); }

var() làm code dễ bảo trì, dễ thay đổi, dễ đọc hơn